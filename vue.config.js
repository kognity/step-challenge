module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'src/public/index.html',
      filename: 'index.html',
    },
  },
};
