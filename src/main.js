import Vue from 'vue';
import App from './App.vue';

import initMap from './map';

window.initMap = initMap;

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');

// Reload every 30min for new code
const reloadInterval = 1000 * 60 * 30;
function reload() {
  window.location.reload();
  setTimeout(reload, reloadInterval);
}
setTimeout(reload, reloadInterval);
