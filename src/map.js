/* global google */

import kognites from './kognites'
import { getColor, teams } from './kognites'

let map;
const ANIMATION_RATE_MS = 20; // 50fps
const SPEED = 700; // Artbitrary, linear speed
const WAIT_TO_ZOOM = 500;
const kognitesCircles = {};
const kognitesMarkers = {};
const teamsCircles = {};
const teamsMarkers = {};
const center = { lat: 59.3348527, lng: 18.0948963 };

const noKognites = Object.keys(kognites).length;
const noTeams = Object.keys(teams).length;
const kognitesAngleDiff = 360 / noKognites;
const teamsAngleDiff = 360 / noTeams;

function sortItemsBySteps(items) {
  return Object.values(items).sort((a, b) => b.steps - a.steps);
}

const classification = sortItemsBySteps(kognites);

function getInitials(name) {
  const kogniteClassification = classification.findIndex((k) => k.name === name);
  if (kogniteClassification > 9) {
    return '';
  }
  const names = name.split(' ');
  const firstAndLast = [names.shift(), names.pop()];
  return firstAndLast.map(w => w[0].toUpperCase()).join('');
}

function getKogSvg() {
  return `
  <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
    <circle r="20" cx="20" cy="20" fill="#4E7A8D"/>
    <circle r="10" cx="15" cy="9" fill="white" clip-path="url(#clipUpperCircle)"/>
    <circle r="10" cx="15" cy="31" fill="white" clip-path="url(#clipLowerCircle)"/>
    <clipPath id="clipUpperCircle">
        <rect width="12" height="10" x="15" y="9"/>
    </clipPath>
    <clipPath id="clipLowerCircle">
        <rect width="12" height="10" x="15" y="21"/>
    </clipPath>
  </svg>
  `;
}

function getSvg(item) {
  return `
  <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
    <style>
      .heavy { font: bold 12px sans-serif; }
    </style>
    <clipPath id="clipCircle">
      <circle r="15" cx="15" cy="15"/>
    </clipPath>
    <clipPath id="clipInnerCircle">
      <circle cx="15" cy="15" r="13"/>
    </clipPath>
    <rect width="30" height="30" fill="${item.color || getColor(item)}" clip-path="url(#clipCircle)"/>
    <text x="7" y="20" class="heavy">${item.initials || getInitials(item.name)}</text>
  </svg>
  `;
}

function stepsToMeters(steps) {
  return steps * 0.6;
}

function stepsToKm(steps) {
  return stepsToMeters(steps) / 1000;
}

function stepsToMetersStr(steps) {
  return stepsToMeters(steps).toFixed(2);
}

export function stepsToKmsStr(steps) {
  return stepsToKm(steps).toFixed(2);
}

export function avgKmsStr(steps, days, members = 1) {
  return (stepsToKm(steps) / days / members).toFixed(2);
}

function createCircle(map, item) {
  return new google.maps.Circle({
    strokeColor: getColor(item),
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.0,
    radius: stepsToMeters(item.steps),
    center,
    map,
  });
}

function getItemPosition(item) {
  return google.maps.geometry.spherical.computeOffset(
    new google.maps.LatLng(center),
    stepsToMeters(item.steps),
    item.position,
  );
}

function getInfoWindowFor(kognite) {
  return new google.maps.InfoWindow({
    content: `
      <div>
        <ul>
          <li><b>Kognite:</b> ${kognite.name}</li>
          <li><b>Steps:</b> ${kognite.steps}</li>
          <li><b>Steps in meters:</b> ${stepsToMetersStr(kognite.steps)}m</li>
          <li><b>Steps in km:</b> ${stepsToKmsStr(kognite.steps)}km</li>
        </ul>
      </div>
    `,
  });
}

function createMarker(map, item) {
  const position = getItemPosition(item);
  const icon = {
    url: `data:image/svg+xml;charset=UTF-8,${encodeURIComponent(getSvg(item))}`,
    scaledSize: new google.maps.Size(30, 30),
    optimized: false,
  };
  const infowindow = getInfoWindowFor(item);
  const marker = new google.maps.Marker({
    animation: google.maps.Animation.DROP,
    position,
    icon,
    map,
  });
  marker.addListener('click', () => {
    infowindow.open(map, marker);
  });
  return marker;
}

function addKogMarker(map) {
  const icon = {
    url: `data:image/svg+xml;charset=UTF-8,${encodeURIComponent(getKogSvg())}`,
    scaledSize: new google.maps.Size(40, 40),
    optimized: false,
    anchor: new google.maps.Point(20, 20),
  };
  return new google.maps.Marker({
    animation: google.maps.Animation.DROP,
    title: 'Kognity HQ',
    position: center,
    icon,
    map,
  });
}

function updateItem(marker, item, angleDiff) {
  item.position += angleDiff / SPEED;
  const newPosition = getItemPosition(item);
  marker.setPosition(newPosition);
}

function animate(items, markers, angleDiff) {
  Object.entries(items).forEach(([itemName, item]) => {
    updateItem(markers[itemName], item, angleDiff) 
  });
  setTimeout(() => animate(items, markers, angleDiff), ANIMATION_RATE_MS);
}

function dropMarkers(markers) {
  Object.values(markers).forEach((marker) => {
    marker.setAnimation(google.maps.Animation.DROP);
  });
}

function initItems(collection, markers, circles, angleDiff) {
  if (!Object.values(markers).length) {
    // Create all markers and circles as they don't exist
    Object.entries(collection).forEach(([itemKey, item], index) => {
      item.position = angleDiff * index;
      circles[itemKey] = createCircle(map, item);
      markers[itemKey] = createMarker(map, item);
    });
    // start animation
    animate(collection, markers, angleDiff);
  } else {
    updateMapObjects(markers, map);
    updateMapObjects(circles, map);
    dropMarkers(markers)
  }
}

function updateMapObjects(mapObjects, map) {
  Object.values(mapObjects).forEach((marker) => {
    marker.setMap(map);
  });
}

function panToBiggerIn(circles, items) {
  const biggerItem = sortItemsBySteps(items)[0];
  const biggerCircle = circles[biggerItem.name];
  setTimeout(() => {
    const oldZoom = map.getZoom();
    map.fitBounds(biggerCircle.getBounds(), {left: 1, right: 1, top: 1, bottom: 1});
    if(oldZoom < map.getZoom()) {
      // map.setZoom(map.getZoom() + 0.8);
    }
  }, WAIT_TO_ZOOM);
}

function clearItems(markers, circles) {
  updateMapObjects(markers, null);
  updateMapObjects(circles, null);
}

export function toggleView(viewTeams) {
  if(viewTeams) {
    clearItems(kognitesMarkers, kognitesCircles);
    initItems(teams, teamsMarkers, teamsCircles, teamsAngleDiff);
    panToBiggerIn(teamsCircles, teams);
  } else {
    clearItems(teamsMarkers, teamsCircles);
    initItems(kognites, kognitesMarkers, kognitesCircles, kognitesAngleDiff);
    panToBiggerIn(kognitesCircles, kognites);
  }
}

export default function initMap() { // eslint-disable-line
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center,
    mapTypeId: 'terrain',
  });

  addKogMarker(map);
}

// setTimeout(clearKognites, 3000);
// setTimeout(initKognites, 6000)
