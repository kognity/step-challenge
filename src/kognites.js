const kognites = {
  'Aliaksei Harabchuk': {
    team: 'Team 1',
    name: 'Aliaksei Harabchuk',
    steps: 55800 + 57747,
  },
  'Carl Emilsson': {
    team: 'Team 1',
    name: 'Carl Emilsson',
    steps: 89580 + 82858,
  },
  'Joel Englund': {
    team: 'Team 1',
    name: 'Joel Englund',
    steps: 54061 + 74123,
  },
  'Marina Hartung': {
    team: 'Team 1',
    name: 'Marina Hartung',
    steps: 37554 + 29581,
  },
  'Niklas Åkesson': {
    team: 'Team 1',
    name: 'Niklas Åkesson',
    steps: 63227 + 48451,
  },
  'Stergios Lazos': {
    team: 'Team 1',
    name: 'Stergios Lazos',
    steps: 22359 + 0,
  },
  'Alice Kelly': {
    team: 'Team 2',
    name: 'Alice Kelly',
    steps: 43484 + 48832,
  },
  'Courtney Librizzi': {
    team: 'Team 2',
    name: 'Courtney Librizzi',
    steps: 51691 + 30598,
  },
  'Jonna Holmberg': {
    team: 'Team 2',
    name: 'Jonna Holmberg',
    steps: 70366 + 81544,
  },
  'Mark Haney': {
    team: 'Team 2',
    name: 'Mark Haney',
    steps: 57869 + 34874,
  },
  'Qiana Johnson': {
    team: 'Team 2',
    name: 'Qiana Johnson',
    steps: 0 + 0,
  },
  'Tessa Dagley': {
    team: 'Team 2',
    name: 'Tessa Dagley',
    steps: 63269 + 48479,
  },
  'Amber Deane': {
    team: 'Team 3',
    name: 'Amber Deane',
    steps: 34458 + 0,
  },
  'Edward Smith': {
    team: 'Team 3',
    name: 'Edward Smith',
    steps: 55658 + 56224,
  },
  'Kakul Gupta': {
    team: 'Team 3',
    name: 'Kakul Gupta',
    steps: 57164 + 63272,
  },
  'Martin Lissmats': {
    team: 'Team 3',
    name: 'Martin Lissmats',
    steps: 117678 + 115764,
  },
  'Sara-Oona Pentikäinen': {
    team: 'Team 3',
    name: 'Sara-Oona Pentikäinen',
    steps: 80349 + 63925,
  },
  'Vinicius Senna': {
    team: 'Team 3',
    name: 'Vinicius Senna',
    steps: 78055 + 70900,
  },
  'Andreas Nordén': {
    team: 'Team 4',
    name: 'Andreas Nordén',
    steps: 45241 + 35571,
  },
  'Harriet Brinton': {
    team: 'Team 4',
    name: 'Harriet Brinton',
    steps: 63063 + 51583,
  },
  'Konstantine Ksygkakis': {
    team: 'Team 4',
    name: 'Konstantine Ksygkakis',
    steps: 67320 + 0,
  },
  'Melodie Emre': {
    team: 'Team 4',
    name: 'Melodie Emre',
    steps: 44396 + 39443,
  },
  'Sofie Knutsson': {
    team: 'Team 4',
    name: 'Sofie Knutsson',
    steps: 49677 + 0,
  },
  'Carl Ballantine': {
    team: 'Team 4',
    name: 'Carl Ballantine',
    steps: 95502 + 80561,
  },
  'Anna Helme': {
    team: 'Team 5',
    name: 'Anna Helme',
    steps: 64598 + 31178,
  },
  'Hugo Wernhoff': {
    team: 'Team 5',
    name: 'Hugo Wernhoff',
    steps: 112014 + 120194,
  },
  'Kristina Lind': {
    team: 'Team 5',
    name: 'Kristina Lind',
    steps: 29136 + 0,
  },
  'Michael Chilcott': {
    team: 'Team 5',
    name: 'Michael Chilcott',
    steps: 56301 + 52311,
  },
  'Sophie Murten': {
    team: 'Team 5',
    name: 'Sophie Murten',
    steps: 61446 + 58870,
  },
  'Anne Remy': {
    team: 'Team 6',
    name: 'Anne Remy',
    steps: 81743 + 102424,
  },
  'Ian Lota': {
    team: 'Team 6',
    name: 'Ian Lota',
    steps: 2441 + 3016,
  },
  'Marcus Erlandsson': {
    team: 'Team 6',
    name: 'Marcus Erlandsson',
    steps: 68030 + 0,
  },
  'Nicholas Johansson': {
    team: 'Team 6',
    name: 'Nicholas Johansson',
    steps: 57750 + 53398,
  },
  'Sophie Malmborg': {
    team: 'Team 6',
    name: 'Sophie Malmborg',
    steps: 51268 + 70181,
  },
};

export const days = 7 * 4;

const colorsByTeam = {
  'Kognity': '#4E7A8D',
  'Team 1': '#4E7A8D',
  'Team 2': '#004E64',
  'Team 3': '#ACFCD9',
  'Team 4': '#F2E86D',
  'Team 5': '#8B1E3F',
  'Team 6': '#ccc',
  'Team 7': '#faa',
}

const teamsInitials = {
  'Lightning Bolts': 'LB',
  'Kogsteppers': 'KS',
  'SQuAD-H': 'SH',
  'The Winning Team': 'WT',
  'Audrey Stepburn': 'AS',
  'Legs Misérables': 'LM',
  'Kogfeet': 'KF',
  'Kognity': 'K',
}

export const teams = Object.values(kognites).reduce((acc, kognite) => {
  if (!acc[kognite.team]) {
    acc[kognite.team] = {
      name: kognite.team,
      color: colorsByTeam[kognite.team],
      steps: 0,
      initials: teamsInitials[kognite.team],
      members: 0,
    };
  }
  acc[kognite.team].steps += kognite.steps;
  acc[kognite.team].members += 1;
  return acc;
}, {});


export function getColor(kognite) {
  return colorsByTeam[kognite.team];
}

export default kognites;
